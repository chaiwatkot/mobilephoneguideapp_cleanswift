//
//  Additional.swift
//  MobileGuide
//
//  Created by Chaiwat Chanthasen on 27/8/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

extension UIImageView {
  func loadImageUrl(_ urlString: String) {
    guard let urls = URL(string: urlString) else { return }
    self.af_setImage(withURL: urls)
  }
}
