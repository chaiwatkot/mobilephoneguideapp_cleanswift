//
//  ListSceneViewController.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 2/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


protocol ListSceneViewControllerInterface: class {
  func displayMobileListOnTable(viewModel: ListScene.GetMobileListData.ViewModel)
  func displaySetDisplayData(viewModel: ListScene.GetMobileListData.ViewModel)
}

class ListSceneViewController: UIViewController, ListSceneViewControllerInterface {
  
  var interactor: ListSceneInteractorInterface?
  var router: ListSceneRouter?
  
  @IBOutlet weak private var mSegment: UISegmentedControl!
  @IBOutlet weak private var mTableView: UITableView!
  
  private var displayListMobile: [ListScene.MobileDataToList] = []
  private var displayListMobileAll: [ListScene.MobileDataToList] = []
  var isDisplayFullList: Bool = false
  // MARK: - Object lifecycle
  
  override func awakeFromNib() {
    super.awakeFromNib()
    configure(viewController: self)
  }
  
  // MARK: - Configuration
  
  private func configure(viewController: ListSceneViewController) {
    let router = ListSceneRouter()
    router.viewController = viewController
    
    let presenter = ListScenePresenter()
    presenter.viewController = viewController
    
    let interactor = ListSceneInteractor()
    interactor.presenter = presenter
    interactor.worker = ListSceneWorker(store: ListSceneStore())
    
    viewController.interactor = interactor
    viewController.router = router
  }
  
  // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setUpSegment()
    registerTableViewCells()
    distplayMobileList()
  }
  
  // MARK: - TableVIew
  
  private func registerTableViewCells() {
    mTableView?.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: "CustomCell")
  }
  
  // MARK: - Event handling
  
  // MARK: - Display logic
  
  func displayMobileListOnTable(viewModel: ListScene.GetMobileListData.ViewModel) {
    // NOTE: Display the result from the Presenter
    displayListMobileAll = viewModel.displayListMobile
    displayListMobile = viewModel.displayListMobile
    changeTab()
  }
  
  func distplayMobileList() {
    let request = ListScene.GetMobileListData.Request()
    interactor?.getMobilelist(request: request)
  }
  
  func displaySetDisplayData(viewModel: ListScene.GetMobileListData.ViewModel) {
    displayListMobile.removeAll()
    displayListMobile = viewModel.displayListMobile
    displayListMobileAll = viewModel.displayListMobile
    changeTab()
  }
  
  // MARK: - IBAction
  @IBAction private func showSortAlert() {
    self.showAlertWithThreeButton()
  }
  
  private func showAlertWithThreeButton() {
    let alert = UIAlertController(title: "Sort", message: nil, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Price low to high", style: .default, handler: { (_) in
      let sortCase = ListScene.sortCase.lowToHigh
      let request = ListScene.SortMobileList.Request(sortCase: sortCase)
      self.interactor?.setSortMobileList(request: request)
    }))
    
    alert.addAction(UIAlertAction(title: "Price high to low", style: .default, handler: { (_) in
      let sortCase = ListScene.sortCase.highToLow
      let request = ListScene.SortMobileList.Request(sortCase: sortCase)
      self.interactor?.setSortMobileList(request: request)
    }))
    
    alert.addAction(UIAlertAction(title: "Rating", style: .default, handler: { (_) in
      let sortCase = ListScene.sortCase.rating
      let request = ListScene.SortMobileList.Request(sortCase: sortCase)
      self.interactor?.setSortMobileList(request: request)
    }))
    alert.addAction(UIAlertAction(title: "Cancle", style: .cancel, handler: nil))
    self.present(alert, animated: true, completion: nil)
  }
  
  func setUpSegment() {
    let titleTextAttributesForSelected = [NSAttributedString.Key.foregroundColor: UIColor.black]
    let titleTextAttributesForUnSelect = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
    UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributesForSelected, for: .selected)
    UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributesForUnSelect, for: .normal)
  }
  
  @IBAction private func changeTab() {
    if mSegment.selectedSegmentIndex == 0 {
      displayListMobile = displayListMobileAll
      mTableView.reloadData()
      isDisplayFullList = false
    } else {
      displayListMobile = displayListMobile.filter {$0.isFavourite == true}
      mTableView.reloadData()
      isDisplayFullList = true
    }
  }
}

extension ListSceneViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return displayListMobile.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as? ListSceneCustomCell else { return UITableViewCell() }
    cell.configureCell(listCell: displayListMobile[indexPath.row], index: indexPath.row)
    cell.setFavouriteButtonHidden(isFullListDisplay: isDisplayFullList)
    cell.delegate = self
    return cell
  }
  
  func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
    if mSegment.selectedSegmentIndex == 1 {
      return .delete
    } else {
      return .none
    }
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    if mSegment.selectedSegmentIndex == 1 {
      mTableView.beginUpdates()
      mTableView.deleteRows(at: [indexPath], with: .automatic)
      let id = displayListMobile[indexPath.row].id
      let index = displayListMobile.firstIndex { $0.id == id } ?? 0
      let request = ListScene.DeleteFavourite.Request(id: id, index: indexPath.row)
      displayListMobile.remove(at: index)
      interactor?.deleteFavourite(request: request)
      mTableView.reloadData()
      mTableView.endUpdates()
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    router?.navigateToDetailScene(index: indexPath.row)
  }
}

extension ListSceneViewController: ListSceneViewCellControllerDelegate {
  func setFavourite(index: Int) {
    let request = ListScene.SetFavourite.Request(index: index)
    interactor?.setIsFavourite(request: request)
  }
  
  func setFavouriteList(index: Int) {
    let isFavourite = interactor?.mobileListResponse[index].isFavourite ?? false
    interactor?.mobileListResponse[index].isFavourite = !isFavourite
    let request = ListScene.reloadSetFavourite.Request()
    interactor?.reloadSetFavourite(requset: request)
    let cell = mTableView.cellForRow(at: IndexPath(row: index, section: 0))
    cell?.reloadInputViews()
  }
}

