//
//  ListSceneRouter.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 2/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

protocol ListSceneRouterInput {
  func navigateToDetailScene(index: Int)
}

class ListSceneRouter: ListSceneRouterInput {
  
  weak var viewController: ListSceneViewController?
  // MARK: - Navigation
  func navigateToDetailScene(index: Int) {
    let storyboard = UIStoryboard(name: "Detail", bundle: nil)
    guard let detailViewController = storyboard.instantiateInitialViewController() as? DetailSceneViewController else { return }
    detailViewController.interactor.mobileDataSelectedRow = viewController?.interactor?.mobileListResponse[index]
    viewController?.navigationController?.pushViewController(detailViewController, animated: true)
  }
}
