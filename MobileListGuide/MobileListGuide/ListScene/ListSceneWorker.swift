//
//  ListSceneWorker.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 2/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

protocol ListSceneStoreProtocol {
  func getData(_ completion: @escaping (StructMobileListresponse) -> Void)
}

class ListSceneWorker {

  var store: ListSceneStoreProtocol

  init(store: ListSceneStoreProtocol) {
    self.store = store
  }
  
  // MARK: - Business Logic
  
  func getMobileListFromApi(_ completion: @escaping (StructMobileListresponse) -> Void) {
    // NOTE: Do the work
    store.getData { (result) in
      completion(result)
    }
  }
}
