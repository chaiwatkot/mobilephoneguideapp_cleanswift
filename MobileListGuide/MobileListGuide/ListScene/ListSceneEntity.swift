//
//  ListSceneEntity.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 2/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import Foundation
import UIKit

/*

 The Entity class is the business object.
 The Result enumeration is the domain model.

 1. Rename this file to the desired name of the model.
 2. Rename the Entity class with the desired name of the model.
 3. Move this file to the Models folder.
 4. If the project already has a declaration of Result enumeration, you may remove the declaration from this file.
 5. Remove the following comments from this file.

 */

typealias StructMobileListresponse = [MobileResponse]

struct MobileResponse: Codable, Equatable {
  let thumbImageURL, brand: String
  let rating: Double
  let id: Int
  let name, description: String
  let price: Double
  var isFavourite: Bool = false

  enum CodingKeys: String, CodingKey {
    case thumbImageURL, brand, name, description, id, rating, price
  }
}

//func ==(lhs: MobileResponse, rhs: MobileResponse) -> Bool
//{
//  return lhs
//}
