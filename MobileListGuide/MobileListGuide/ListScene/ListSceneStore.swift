//
//  ListSceneStore.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 2/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import Foundation
import Alamofire
/*
 
 The ListSceneStore class implements the ListSceneStoreProtocol.
 
 The source for the data could be a database, cache, or a web service.
 
 You may remove these comments from the file.
 
 */
class ListSceneStore: ListSceneStoreProtocol {
  private let url = "https://scb-test-mobile.herokuapp.com/api/mobiles/"
  func getData(_ completion: @escaping (StructMobileListresponse) -> Void) {
    guard let urls = URL(string: url) else { return }
      AF.request(urls, method: .get).responseJSON { response in
        guard let data = response.data else { return }
        switch response.result {
        case .success:
          print(response.result)
          do {
            let decoder = JSONDecoder()
            let result = try decoder.decode(StructMobileListresponse.self, from: data)
            completion(result)
          } catch let error {
            print(error)
          }
        case let .failure(error):
          completion([])
          print(error)
        }
      }
  }
}
