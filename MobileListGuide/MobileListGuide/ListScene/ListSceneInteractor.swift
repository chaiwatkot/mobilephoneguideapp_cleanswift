//
//  ListSceneInteractor.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 2/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

protocol ListSceneInteractorInterface {
  func getMobilelist(request: ListScene.GetMobileListData.Request)
  func setIsFavourite(request: ListScene.SetFavourite.Request)
  func setSortMobileList(request: ListScene.SortMobileList.Request)
  func reloadSetFavourite(requset: ListScene.reloadSetFavourite.Request)
  func deleteFavourite(request: ListScene.DeleteFavourite.Request)
  var mobileListResponse: StructMobileListresponse { get set }
}

class ListSceneInteractor: ListSceneInteractorInterface {
  
  var mobileListResponse: StructMobileListresponse = []
  var presenter: ListScenePresenterInterface?
  var worker: ListSceneWorker?
  
  // MARK: - Business logic
  
  func getMobilelist(request: ListScene.GetMobileListData.Request) {
    worker?.getMobileListFromApi { (result) in
      self.mobileListResponse = result
      let response = ListScene.GetMobileListData.Response(mobileListResponse: self.mobileListResponse)
      self.presenter?.presentMobileList(response: response)
    }
    // NOTE: Pass the result to the Presenter. This is done by creating a response model with the result from the worker. The response could contain a type like UserResult enum (as declared in the SCB Easy project) with the result as an associated value.
  }
  
  func setIsFavourite(request: ListScene.SetFavourite.Request) {
    self.mobileListResponse[request.index].isFavourite = !(self.mobileListResponse[request.index].isFavourite)
    let response = ListScene.SetFavourite.Response(mobileListResponse: self.mobileListResponse)
    self.presenter?.presentMobileListIsFavourite(response: response)
  }
  
  func reloadSetFavourite(requset: ListScene.reloadSetFavourite.Request) {
    let response = ListScene.reloadSetFavourite.Response(mobileListResponse: self.mobileListResponse)
    self.presenter?.presentSetMobileDataIsFavourite(response: response)
  }
  
  func setSortMobileList(request: ListScene.SortMobileList.Request) {
    switch request.sortCase {
    case .lowToHigh:
      self.mobileListResponse.sort { (fisrt, second) -> Bool in
        fisrt.price < second.price
      }
      let response = ListScene.SortMobileList.Response(mobileListResponse: self.mobileListResponse)
      presenter?.presentMobileListIsSort(response: response)
    case .highToLow:
      self.mobileListResponse.sort { (fisrt, second) -> Bool in
        fisrt.price > second.price
      }
      let response = ListScene.SortMobileList.Response(mobileListResponse: self.mobileListResponse)
      presenter?.presentMobileListIsSort(response: response)
    case .rating:
      self.mobileListResponse.sort { (fisrt, second) -> Bool in
        fisrt.rating > second.rating
      }
      let response = ListScene.SortMobileList.Response(mobileListResponse: self.mobileListResponse)
      presenter?.presentMobileListIsSort(response: response)
    }
  }
  
  func deleteFavourite(request: ListScene.DeleteFavourite.Request) {
    if let index = mobileListResponse.firstIndex(where: {$0.id == request.id}) {
      mobileListResponse[index].isFavourite = !(mobileListResponse[index].isFavourite)
      let response = ListScene.reloadSetFavourite.Response(mobileListResponse: self.mobileListResponse)
      self.presenter?.presentSetMobileDataIsFavourite(response: response)
    }
  }
}

