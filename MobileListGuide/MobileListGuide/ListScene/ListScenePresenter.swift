//
//  ListScenePresenter.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 2/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

protocol ListScenePresenterInterface {
  func presentMobileList(response: ListScene.GetMobileListData.Response)
  func presentMobileListIsFavourite(response: ListScene.SetFavourite.Response)
  func presentMobileListIsSort(response: ListScene.SortMobileList.Response)
  func presentSetMobileDataIsFavourite(response: ListScene.reloadSetFavourite.Response)
}

class ListScenePresenter: ListScenePresenterInterface {
  
  weak var viewController: ListSceneViewControllerInterface!
  private var displayListMobiles: [ListScene.MobileDataToList] = []

  // MARK: - Presentation logic
  
  func presentMobileList(response: ListScene.GetMobileListData.Response) {
    displayListMobiles.removeAll()
    for mobileData in response.mobileListResponse {
      let displayListMobile = ListScene.MobileDataToList(thumbImageURL: mobileData.thumbImageURL,
                                                         brand: mobileData.brand,
                                                         rating: "Rating: \(mobileData.rating)",
                                                         id: mobileData.id, name: mobileData.name,
                                                         description: mobileData.description,
                                                         price: "Price: \(mobileData.price)",
                                                         isFavourite: mobileData.isFavourite)
      displayListMobiles.append(displayListMobile)
    }
    let viewModel = ListScene.GetMobileListData.ViewModel(displayListMobile: displayListMobiles)
    viewController.displayMobileListOnTable(viewModel: viewModel)
  }
  
  func presentMobileListIsFavourite(response: ListScene.SetFavourite.Response) {
    displayListMobiles.removeAll()
    for mobileData in response.mobileListResponse {
      let displayListMobile = ListScene.MobileDataToList(thumbImageURL: mobileData.thumbImageURL,
                                                         brand: mobileData.brand,
                                                         rating: "Rating: \(mobileData.rating)",
                                                         id: mobileData.id,
                                                         name: mobileData.name,
                                                         description: mobileData.description,
                                                         price: "Price: \(mobileData.price)",
                                                         isFavourite: mobileData.isFavourite)
      displayListMobiles.append(displayListMobile)
    }
    let viewModel = ListScene.GetMobileListData.ViewModel(displayListMobile: displayListMobiles)
    viewController.displayMobileListOnTable(viewModel: viewModel)
  }
  
  func presentMobileListIsSort(response: ListScene.SortMobileList.Response) {
    displayListMobiles.removeAll()
    for mobileData in response.mobileListResponse {
      let displayListMobile = ListScene.MobileDataToList(thumbImageURL: mobileData.thumbImageURL,
                                                         brand: mobileData.brand,
                                                         rating: "Rating: \(mobileData.rating)",
                                                         id: mobileData.id,
                                                         name: mobileData.name,
                                                         description: mobileData.description,
                                                         price: "Price: \(mobileData.price)",
                                                         isFavourite: mobileData.isFavourite)
      displayListMobiles.append(displayListMobile)
    }
    let viewModel = ListScene.GetMobileListData.ViewModel(displayListMobile: displayListMobiles)
    viewController.displayMobileListOnTable(viewModel: viewModel)
  }
  
  func presentSetMobileDataIsFavourite(response: ListScene.reloadSetFavourite.Response) {
    buildModel(response: response)
    let viewModel = ListScene.GetMobileListData.ViewModel(displayListMobile: displayListMobiles)
    viewController.displaySetDisplayData(viewModel: viewModel)
  }
  
  private func buildModel(response: ListScene.reloadSetFavourite.Response) {
    displayListMobiles = response.mobileListResponse.map {
      ListScene.MobileDataToList(thumbImageURL: $0.thumbImageURL,
                                 brand: $0.brand,
                                 rating: "Rating: \($0.rating)",
                                 id: $0.id,
                                 name: $0.name,
                                 description: $0.description,
                                 price: "Price: \($0.price)",
                                 isFavourite: $0.isFavourite)
    }
  }
}
