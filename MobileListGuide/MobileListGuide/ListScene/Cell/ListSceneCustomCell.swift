//
//  CustomTableView.swift
//  MobileGuide
//
//  Created by Chaiwat Chanthasen on 27/8/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

protocol ListSceneViewCellControllerDelegate: class {
  func setFavourite(index: Int)
  func setFavouriteList(index: Int)
}

class ListSceneCustomCell: UITableViewCell {
  
  weak var viewController: ListSceneViewControllerInterface?

  @IBOutlet weak private var title: UILabel!
  @IBOutlet weak private var descriptions: UILabel!
  @IBOutlet weak private var price: UILabel!
  @IBOutlet weak private var rating: UILabel!
  @IBOutlet weak private var imageViews: UIImageView!
  @IBOutlet weak private var button: UIButton!
  
  weak var delegate: ListSceneViewCellControllerDelegate?
  
  private var index: Int?
  private var listCell: ListScene.MobileDataToList? {
    didSet {
      title.text = listCell?.name
      descriptions.text = listCell?.description
      price.text = listCell?.price
      rating.text = listCell?.rating
      imageViews.loadImageUrl(listCell?.thumbImageURL ?? "")
      if listCell?.isFavourite ?? false {
        button.setImage(UIImage(named: "star2.png"), for: .normal)
      } else {
        button.setImage(UIImage(named: "star1.png"), for: .normal)
      }
    }
  }
  
  func configureCell(listCell: ListScene.MobileDataToList, index: Int) {
    self.listCell = listCell
    self.index = index
  }
  
  func setFavouriteButtonHidden(isFullListDisplay: Bool) {
    button.isHidden = isFullListDisplay
  }
  
  @IBAction func setIsFavouriteList() {
    guard let isFavourite = listCell?.isFavourite, let index = index else { return }
    listCell?.isFavourite = !isFavourite
    delegate?.setFavouriteList(index: index)
  }
}
