//
//  ListSceneModels.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 2/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

struct ListScene {
  /// This structure represents a use case
  
  struct MobileDataToList: Equatable {
    let thumbImageURL, brand: String
    let rating: String
    let id: Int
    let name, description: String
    let price: String
    var isFavourite: Bool = false
  }
  
  enum sortCase {
    case lowToHigh
    case highToLow
    case rating
  }
  
  struct GetMobileListData {
    /// Data struct sent to Interactor
    struct Request {
    }
    /// Data struct sent to Presenter
    struct Response {
      var mobileListResponse: StructMobileListresponse
    }
    /// Data struct sent to ViewController
    struct ViewModel {
      var displayListMobile: [MobileDataToList]
    }
  }
  
  struct SortMobileList {
    /// Data struct sent to Interactor
    struct Request {
      var sortCase: sortCase
    }
    /// Data struct sent to Presenter
    struct Response {
      var mobileListResponse: StructMobileListresponse
    }
    /// Data struct sent to ViewController
    struct ViewModel {
      var displayListMobile: [MobileDataToList]
    }
  }
 
  struct SetFavourite {
    /// Data struct sent to Interactor
    struct Request {
      var index: Int
    }
    /// Data struct sent to Presenter
    struct Response {
      var mobileListResponse: StructMobileListresponse
    }
    /// Data struct sent to ViewController
    struct ViewModel {
      var displayListMobile: [MobileDataToList]
    }
  }
  
  struct reloadSetFavourite {
    /// Data struct sent to Interactor
    struct Request {
    }
    /// Data struct sent to Presenter
    struct Response {
      var mobileListResponse: StructMobileListresponse
    }
    /// Data struct sent to ViewController
    struct ViewModel {
      var displayListMobile: [MobileDataToList]
    }
  }
  
  struct DeleteFavourite {
    /// Data struct sent to Interactor
    struct Request {
      var id: Int
      var index: Int
    }
    /// Data struct sent to Presenter
    struct Response {
      var mobileListResponse: StructMobileListresponse
    }
    /// Data struct sent to ViewController
    struct ViewModel {
      var displayListMobile: [MobileDataToList]
    }
  }
}
