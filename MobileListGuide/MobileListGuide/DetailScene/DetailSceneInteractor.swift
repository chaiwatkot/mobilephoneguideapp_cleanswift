//
//  DetailSceneInteractor.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 3/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

protocol DetailSceneInteractorInterface {
  func getMobileImagesFromAPI(request: DetailScene.getImagesWithIdFromAPI.Request)
  func getMobileDataFromFullList(request: DetailScene.setDetailMobile.Request)
  var mobileDataSelectedRow: MobileResponse? { get set }
}

class DetailSceneInteractor: DetailSceneInteractorInterface {
  var presenter: DetailScenePresenterInterface?
  var worker: DetailSceneWorker?
  private var mobileImagesGetByID: ResponseMobileImagesFromAPI = []
  var mobileDataSelectedRow: MobileResponse?
  
  // MARK: - Business logic
  
  func getMobileImagesFromAPI(request: DetailScene.getImagesWithIdFromAPI.Request) {
    worker?.getMobileImagesFromApi(id: request.id) { (result) in
      self.mobileImagesGetByID = result
      let response = DetailScene.getImagesWithIdFromAPI.Response(mobileImagesByIdResponse: self.mobileImagesGetByID)
      self.presenter?.presentMobileImages(response: response)
    }
  }
  
  func getMobileDataFromFullList(request: DetailScene.setDetailMobile.Request) {
    if let mobileDataSelectedRow = mobileDataSelectedRow {
    let response = DetailScene.setDetailMobile.Response(mobileListResponse: mobileDataSelectedRow)
      presenter?.presentMobileDetail(response: response)
    }
  }

}
