//
//  DetailSceneRouter.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 3/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

protocol DetailSceneRouterInput {
  func navigateToSomewhere()
  var dataStore: DetailSceneInteractorInterface? { get }

}

class DetailSceneRouter: DetailSceneRouterInput {
  weak var viewController: DetailSceneViewController!
  var dataStore: DetailSceneInteractorInterface?

  // MARK: - Navigation

  func navigateToSomewhere() {

  }

  // MARK: - Communication

  func passDataToNextScene(segue: UIStoryboardSegue) {
    // NOTE: Teach the router which scenes it can communicate with

    if segue.identifier == "ShowSomewhereScene" {
      passDataToSomewhereScene(segue: segue)
    }
  }

  func passDataToSomewhereScene(segue: UIStoryboardSegue) {
   
  }
}
