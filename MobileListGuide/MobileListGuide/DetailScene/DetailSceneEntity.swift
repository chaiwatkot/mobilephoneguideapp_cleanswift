//
//  DetailSceneEntity.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 3/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import Foundation

/*

 The Entity class is the business object.
 The Result enumeration is the domain model.

 1. Rename this file to the desired name of the model.
 2. Rename the Entity class with the desired name of the model.
 3. Move this file to the Models folder.
 4. If the project already has a declaration of Result enumeration, you may remove the declaration from this file.
 5. Remove the following comments from this file.

 */

enum Results<T> {
  case success(T)
  case failure(Error)
}

//
// The entity or business object
//
struct Entitys {}

typealias StructureResponseToDetail = [PurpleStructureResponseToDetail]

struct PurpleStructureResponseToDetail: Codable, Equatable{
  let thumbImageURL, brand: String
  let rating: Double
  let id: Int
  let name, description: String
  let price: Double
  var isFavourite: Bool = false
  
  enum CodingKeys: String, CodingKey {
    case thumbImageURL, brand, name, description, id, rating, price
  }
}

typealias ResponseMobileImagesFromAPI = [MobileImagesFromAPI]

struct MobileImagesFromAPI: Codable, Equatable {
  let mobileID: Int
  var url: String
  let id: Int
  
  enum CodingKeys: String, CodingKey {
    case mobileID = "mobile_id"
    case url, id
  }
}

