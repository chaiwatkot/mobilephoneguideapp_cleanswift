//
//  ImageCollectionCell.swift
//  MobileGuide
//
//  Created by Chaiwat Chanthasen on 27/8/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

import Foundation
import UIKit

class ImageCollectionCell: UICollectionViewCell {
  
  @IBOutlet weak var imagesMobile: UIImageView!
  
}
