//
//  DetailSceneStore.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 3/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import Foundation
import Alamofire
/*
 
 The DetailSceneStore class implements the DetailSceneStoreProtocol.
 
 The source for the data could be a database, cache, or a web service.
 
 You may remove these comments from the file.
 
 */

class DetailSceneStore: DetailSceneStoreProtocol {
  
  func getMobileImagesFromAPI(id: Int, _ completion: @escaping (ResponseMobileImagesFromAPI) -> Void) {
    let id = id
    let url = "https://scb-test-mobile.herokuapp.com/api/mobiles/\(id)/images/"
    if let urls = URL(string: url) {
      AF.request(urls, method: .get).responseJSON { response in
        switch response.result {
        case .success:
          print(response.result)
          do {
            let decoder = JSONDecoder()
            if let data = response.data {
              let result = try decoder.decode(ResponseMobileImagesFromAPI.self, from: data)
              completion(result)
            }
          } catch let error {
            print(error)
          }
        case let .failure(error):
          print(error)
        }
      }
    }
  }
}
