//
//  DetailSceneViewController.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 3/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

protocol DetailSceneViewControllerInterface: class {
  func displayMobileImages(viewModel: DetailScene.getImagesWithIdFromAPI.ViewModel)
  func displayGetMobileDetail(viewModel: DetailScene.setDetailMobile.ViewModel)
}

class DetailSceneViewController: UIViewController, DetailSceneViewControllerInterface {
  var interactor: DetailSceneInteractorInterface!
  var router: DetailSceneRouter!
  private var displayMobileImagesFromApi: [DetailScene.getImagesWithIdFromAPI.ViewModel.MobileImagesFromAPI] = []
  private var mobileDetailData: DetailScene.setDetailMobile.ViewModel.MobileDataToList?
  
  @IBOutlet weak private var collectionView: UICollectionView!
  @IBOutlet weak private var descriptionDetail: UILabel!
  @IBOutlet weak private var price: UILabel!
  @IBOutlet weak private var rating: UILabel!
  @IBOutlet weak private var titleNavigation: UINavigationItem!
  
  // MARK: - Object lifecycle
  
  override func awakeFromNib() {
    super.awakeFromNib()
    configure(viewController: self)
  }
  
  // MARK: - Configuration
  
  private func configure(viewController: DetailSceneViewController) {
    let router = DetailSceneRouter()
    router.viewController = viewController
    
    let presenter = DetailScenePresenter()
    presenter.viewController = viewController
    
    let interactor = DetailSceneInteractor()
    interactor.presenter = presenter
    interactor.worker = DetailSceneWorker(store: DetailSceneStore())
    
    viewController.interactor = interactor
    viewController.router = router
  }
  
  // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView.delegate = self
    collectionView.dataSource = self
    displayImagesMobileById()
    displayMobileDetail()
  }
  
  // MARK: - Event handling
  
  func displayImagesMobileById() {
    let id = interactor.mobileDataSelectedRow?.id ?? 0
    let request = DetailScene.getImagesWithIdFromAPI.Request(id: id)
    interactor.getMobileImagesFromAPI(request: request)
  }
  
  func displayMobileImages(viewModel: DetailScene.getImagesWithIdFromAPI.ViewModel) {
    displayMobileImagesFromApi = viewModel.mobileImagesFromAPI
    self.collectionView.reloadData()
  }
  
  func displayMobileDetail() {
    let request = DetailScene.setDetailMobile.Request()
    interactor.getMobileDataFromFullList(request: request)
  }
  
  func displayGetMobileDetail(viewModel: DetailScene.setDetailMobile.ViewModel) {
    mobileDetailData = viewModel.displayListMobile
    rating.text = mobileDetailData?.rating
    price.text = mobileDetailData?.price
    descriptionDetail.text = mobileDetailData?.description
    titleNavigation.title = mobileDetailData?.name
  }
  
  // MARK: - Display logic
  
  // MARK: - Router
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    router.passDataToNextScene(segue: segue)
  }
  
  @IBAction func unwindToDetailSceneViewController(from segue: UIStoryboardSegue) {
    print("unwind...")
    router.passDataToNextScene(segue: segue)
  }
}


extension DetailSceneViewController: UICollectionViewDelegate, UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return displayMobileImagesFromApi.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionCell", for: indexPath) as? ImageCollectionCell else { return UICollectionViewCell() }
    let item = displayMobileImagesFromApi[indexPath.row]
    cell.imagesMobile.loadImageUrl(item.url)
    return cell
  }
}
