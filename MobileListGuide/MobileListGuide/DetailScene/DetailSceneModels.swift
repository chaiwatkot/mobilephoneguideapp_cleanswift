//
//  DetailSceneModels.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 3/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

struct DetailScene {
  /// This structure represents a use case
  struct getImagesWithIdFromAPI {
    /// Data struct sent to Interactor
    struct Request {
      var id: Int
    }
    /// Data struct sent to Presenter
    struct Response {
      var mobileImagesByIdResponse: [MobileImagesFromAPI]
    }
    /// Data struct sent to ViewController
    struct ViewModel {
      struct MobileImagesFromAPI: Codable, Equatable {
        let mobileID: Int
        var url: String
        let id: Int
        
        enum CodingKeys: String, CodingKey {
          case mobileID = "mobile_id"
          case url, id
        }
      }
      var mobileImagesFromAPI: [MobileImagesFromAPI]
    }
  }
  
  struct setDetailMobile {
    /// Data struct sent to Interactor
    struct Request {
    }
    /// Data struct sent to Presenter
    struct Response {
      var mobileListResponse: MobileResponse
    }
    /// Data struct sent to ViewController
    struct ViewModel {
      struct MobileDataToList: Equatable {
        let thumbImageURL, brand: String
        let rating: String
        var id: Int
        let name, description: String
        let price: String
        var isFavourite: Bool = false
      }
      var displayListMobile: MobileDataToList
    }
  }
}
