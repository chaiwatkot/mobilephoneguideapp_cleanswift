//
//  DetailScenePresenter.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 3/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

protocol DetailScenePresenterInterface {
  func presentMobileImages(response: DetailScene.getImagesWithIdFromAPI.Response)
  func presentMobileDetail(response: DetailScene.setDetailMobile.Response)
}

class DetailScenePresenter: DetailScenePresenterInterface {
  weak var viewController: DetailSceneViewControllerInterface!
  var mobileDetailDataSetHTTP: [MobileImagesFromAPI] = []
  
  // MARK: - Presentation logic
  
  func presentMobileImages(response: DetailScene.getImagesWithIdFromAPI.Response) {
    // NOTE: Format the response from the Interactor and pass the result back to the View Controller. The resulting view model should be using only primitive types. Eg: the view should not need to involve converting date object into a formatted string. The formatting is done here.
    mobileDetailDataSetHTTP.removeAll()
    for var item in response.mobileImagesByIdResponse {
      item.url = checkHTTP(url: item.url)
      mobileDetailDataSetHTTP.append(item)
    }
    let displayMobileImage = mobileDetailDataSetHTTP.map {
      DetailScene.getImagesWithIdFromAPI.ViewModel.MobileImagesFromAPI(mobileID: $0.mobileID, url: $0.url, id: $0.id)
    }
    let viewModel = DetailScene.getImagesWithIdFromAPI.ViewModel(mobileImagesFromAPI: displayMobileImage)
    viewController.displayMobileImages(viewModel: viewModel)
  }
  
  func presentMobileDetail(response: DetailScene.setDetailMobile.Response) {
    let mobileData = response.mobileListResponse
    let displayMobileImage = DetailScene.setDetailMobile.ViewModel.MobileDataToList(thumbImageURL: mobileData.thumbImageURL,
                                                                                    brand: mobileData.brand,
                                                                                    rating: "Rating: \(mobileData.rating)",
                                                                                    id: mobileData.id, name: mobileData.name,
                                                                                    description: mobileData.description,
                                                                                    price: "Price: \(mobileData.price)",
                                                                                    isFavourite: mobileData.isFavourite)
    let viewModel = DetailScene.setDetailMobile.ViewModel(displayListMobile: displayMobileImage)
    viewController.displayGetMobileDetail(viewModel: viewModel)
  }

  func isValidHTTP(url: String) -> Bool {
    let head1 = "((http|https)://)"
    let head = "([(w|W)]{3}+\\.)?"
    let tail = "\\.+[A-Za-z]{2,3}+(\\.)?+(/(.)*)?"
    let urlRegEx = head1 + head + "+(.)+" + tail
    let httpTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
    return httpTest.evaluate(with: url)
  }
  
  func checkHTTP(url: String) -> String {
    var link: String = url
    if !isValidHTTP(url: url) {
      link = "https://\(url)"
    }
    return link
  }

}
