//
//  DetailSceneWorker.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 3/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

protocol DetailSceneStoreProtocol {
  func getMobileImagesFromAPI(id: Int, _ completion: @escaping (ResponseMobileImagesFromAPI) -> Void)
}

class DetailSceneWorker {

  var store: DetailSceneStoreProtocol

  init(store: DetailSceneStoreProtocol) {
    self.store = store
  }

  // MARK: - Business Logic

  func getMobileImagesFromApi(id: Int, _ completion: @escaping (ResponseMobileImagesFromAPI) -> Void) {
    // NOTE: Do the work
    store.getMobileImagesFromAPI(id: id) { (result) in
      completion(result)
    }
  }
}
