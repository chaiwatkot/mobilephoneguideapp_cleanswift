//
//  DetailSceneInteractorTests.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 5/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

@testable import MobileListGuide
import XCTest

class DetailSceneInteractorTests: XCTestCase {

  // MARK: - Subject under test

  var sut: DetailSceneInteractor!
  var workerSpy: DetailSceneWorkerSpy!
  var presentationSpy: DetailScenePresenterSpy!
  // MARK: - Test lifecycle

  override func setUp() {
    super.setUp()
    setupDetailSceneInteractor()
    presentationSpy = DetailScenePresenterSpy()
    sut.presenter = presentationSpy
    workerSpy = DetailSceneWorkerSpy(store: DetailSceneStore())
    sut.worker = workerSpy
  }

  override func tearDown() {
    super.tearDown()
  }

  // MARK: - Test setup

  func setupDetailSceneInteractor() {
    sut = DetailSceneInteractor()
  }

  // MARK: - Test doubles
  class DetailScenePresenterSpy: DetailScenePresenterInterface {
    
    var presentMobileImagesCalled = false
    var presentMobileDetailCalled = false
    
    var responsePresentMobileImages: DetailScene.getImagesWithIdFromAPI.Response!
    var responsePresentMobileDetail: DetailScene.setDetailMobile.Response!
    
    func presentMobileImages(response: DetailScene.getImagesWithIdFromAPI.Response) {
      presentMobileImagesCalled = true
      responsePresentMobileImages = response
    }
    
    func presentMobileDetail(response: DetailScene.setDetailMobile.Response) {
      presentMobileDetailCalled = true
      responsePresentMobileDetail = response
    }
  }
  
  class DetailSceneWorkerSpy: DetailSceneWorker {
    
    var getMobileImagesFromAPICalled = false
    var forcefailure = false
    
    override func getMobileImagesFromApi(id: Int, _ completion: @escaping (ResponseMobileImagesFromAPI) -> Void) {
      getMobileImagesFromAPICalled = true
      if forcefailure {
        completion([])
      } else {
        completion(Seeds.MobileList.imagesMobile)
      }
    }
  }
  // MARK: - Tests

  func testGetMobileImagesFromAPIandPresentMobileImagesCalled() {
    // Given
    // When
    let request = DetailScene.getImagesWithIdFromAPI.Request(id: 2)
    sut.getMobileImagesFromAPI(request: request)
    // Then
    let responseGetMobileImagesFromApi = presentationSpy.responsePresentMobileImages.mobileImagesByIdResponse
    XCTAssert(responseGetMobileImagesFromApi == Seeds.MobileList.imagesMobile)
    XCTAssert(presentationSpy.presentMobileImagesCalled)
    XCTAssert(workerSpy.getMobileImagesFromAPICalled)
  }
  
  func testGetMobileImagesFromAPIFailure() {
    // Given
    workerSpy.forcefailure = true
    // When
    let request = DetailScene.getImagesWithIdFromAPI.Request(id: 2)
    sut.getMobileImagesFromAPI(request: request)
    // Then
    let responseGetMobileImagesFromApi = presentationSpy.responsePresentMobileImages.mobileImagesByIdResponse
    XCTAssert(responseGetMobileImagesFromApi == [])
    XCTAssert(presentationSpy.presentMobileImagesCalled)
    XCTAssert(workerSpy.getMobileImagesFromAPICalled)
  }
  
  func testMobileDetail() {
    // Given
    sut.mobileDataSelectedRow = Seeds.MobileList.motoE4Plus
    // When
    let request = DetailScene.setDetailMobile.Request()
    sut.getMobileDataFromFullList(request: request)
    // Then
    let responseMobileDetail = presentationSpy.responsePresentMobileDetail.mobileListResponse
    XCTAssert(responseMobileDetail == Seeds.MobileList.motoE4Plus)
    XCTAssert(presentationSpy.presentMobileDetailCalled)
  }
}
