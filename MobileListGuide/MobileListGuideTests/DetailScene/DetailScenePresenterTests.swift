//
//  DetailScenePresenterTests.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 5/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

@testable import MobileListGuide
import XCTest

class DetailScenePresenterTests: XCTestCase {

  // MARK: - Subject under test

  var sut: DetailScenePresenter!
  var displayDetailSpy: DetailSceneDisplaySpy!
  var mobileImages: [MobileImagesFromAPI]!
  var mobiledataDetail: MobileResponse!

  // MARK: - Test lifecycle

  override func setUp() {
    super.setUp()
    setupDetailScenePresenter()
    displayDetailSpy = DetailSceneDisplaySpy()
    sut.viewController = displayDetailSpy
    mobileImages = [Seeds.MobileList.image1ID2, Seeds.MobileList.image2ID2, Seeds.MobileList.image3ID2]
    mobiledataDetail = Seeds.MobileList.motoE4Plus
  }

  override func tearDown() {
    super.tearDown()
  }

  // MARK: - Test setup

  func setupDetailScenePresenter() {
    sut = DetailScenePresenter()
  }

  // MARK: - Test doubles
  class DetailSceneDisplaySpy: DetailSceneViewControllerInterface {
    
    var displayMobileImagesCalled = false
    var displayGetMobileDetail = false
    
    var viewModelDisplayMobileImages: DetailScene.getImagesWithIdFromAPI.ViewModel!
    var viewModelDisplayGetMobileDetail: DetailScene.setDetailMobile.ViewModel!
    
    func displayMobileImages(viewModel: DetailScene.getImagesWithIdFromAPI.ViewModel) {
      displayMobileImagesCalled = true
      viewModelDisplayMobileImages = viewModel
    }
    
    func displayGetMobileDetail(viewModel: DetailScene.setDetailMobile.ViewModel) {
      displayMobileImagesCalled = true
      viewModelDisplayGetMobileDetail = viewModel
    }
  }
  // MARK: - Tests

  func testPresenterDisplayMobileImagesAskViewController() {
    // Given
    // When
    let response = DetailScene.getImagesWithIdFromAPI.Response(mobileImagesByIdResponse: mobileImages)
    sut.presentMobileImages(response: response)
    // Then
    let displayMobileImage = displayDetailSpy.viewModelDisplayMobileImages.mobileImagesFromAPI
    XCTAssert(displayMobileImage[0] == Seeds.MobileList.image1ID2vm)
    XCTAssert(displayMobileImage[1] == Seeds.MobileList.image2ID2vm)
    XCTAssert(displayMobileImage[2] == Seeds.MobileList.image3ID2vm)
    XCTAssert(displayDetailSpy.displayMobileImagesCalled)
  }
  
  func testPresentDisplayMobileDetail() {
    // Given
    // When
    let response = DetailScene.setDetailMobile.Response(mobileListResponse: mobiledataDetail)
    sut.presentMobileDetail(response: response)
    // Then
    let displayMobileDetail = displayDetailSpy.viewModelDisplayGetMobileDetail.displayListMobile
    XCTAssert(displayMobileDetail == Seeds.MobileList.motoE4PlusDetail)
    XCTAssert(displayDetailSpy.displayMobileImagesCalled)
  }

}
