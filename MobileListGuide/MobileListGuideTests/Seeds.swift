
@testable import MobileListGuide
import XCTest

struct Seeds {
  struct MobileList
  {
    static let motoE4Plus = MobileResponse(thumbImageURL: "https://cdn.mos.cms.futurecdn.net/grwJkAGWQp4EPpWA3ys3YC-650-80.jpg", brand: "Samsung", rating: 4.9, id: 1, name: "Moto E4 Plus", description: "First place in our list goes to the excellent Moto E4 Plus. It's a cheap phone that features phenomenal battery life, a fingerprint scanner and a premium feel design, plus it's a lot cheaper than the Moto G5 below. It is a little limited with its power, but it makes up for it by being able to last for a whole two days from a single charge. If price and battery are the most important features for you, the Moto E4 Plus will suit you perfectly.", price: 179.99, isFavourite: false)
    
    static let motoG5 = MobileResponse(thumbImageURL: "https://cdn.mos.cms.futurecdn.net/DcUtY6YfhoajHnoKUgGFqn-650-80.jpg", brand: "Motorola", rating: 3.8, id: 4, name: "Moto G5", description:  "Motorola's Moto G5, a former best cheap phone in the world, has slipped a few places thanks to better priced alternatives, plus the introduction of the new G5S. The Moto G5 comes with a metal design, 1080p display and fingerprint scanner. You won't get the fastest chipset on this list or NFC with the Moto G5, but as an all-round product the cheap Motorola phone will serve you well.", price: 165, isFavourite: false)
    
    static let nokia6 = MobileResponse(thumbImageURL: "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg", brand: "Nokia", rating: 4.6, id: 2, name: "Nokia 6", description: "Nokia is back in the mobile phone game and after a small price drop to the Nokia 6 we've now seen it enter our best cheap phone list. It comes with a Full HD 5.5-inch display, full metal design and a fingerprint scanner for added security. The battery isn't incredible on the Nokia 6, but it's not awful either making this one of our favorite affordable phones on the market right now.", price: 199.9, isFavourite: false)
    
    static let motoE4PlusDisplay = ListScene.MobileDataToList(thumbImageURL: "https://cdn.mos.cms.futurecdn.net/grwJkAGWQp4EPpWA3ys3YC-650-80.jpg", brand: "Samsung", rating: "Rating: 4.9", id: 1, name: "Moto E4 Plus", description: "First place in our list goes to the excellent Moto E4 Plus. It's a cheap phone that features phenomenal battery life, a fingerprint scanner and a premium feel design, plus it's a lot cheaper than the Moto G5 below. It is a little limited with its power, but it makes up for it by being able to last for a whole two days from a single charge. If price and battery are the most important features for you, the Moto E4 Plus will suit you perfectly.", price: "Price: 179.99", isFavourite: false)
    
    static let motoG5Display = ListScene.MobileDataToList(thumbImageURL: "https://cdn.mos.cms.futurecdn.net/DcUtY6YfhoajHnoKUgGFqn-650-80.jpg", brand: "Motorola", rating: "Rating: 3.8", id: 4, name: "Moto G5", description:  "Motorola's Moto G5, a former best cheap phone in the world, has slipped a few places thanks to better priced alternatives, plus the introduction of the new G5S. The Moto G5 comes with a metal design, 1080p display and fingerprint scanner. You won't get the fastest chipset on this list or NFC with the Moto G5, but as an all-round product the cheap Motorola phone will serve you well.", price: "Price: 165.0", isFavourite: false)
    
    static let nokia6Display = ListScene.MobileDataToList(thumbImageURL: "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg", brand: "Nokia", rating: "Rating: 4.6", id: 2, name: "Nokia 6", description: "Nokia is back in the mobile phone game and after a small price drop to the Nokia 6 we've now seen it enter our best cheap phone list. It comes with a Full HD 5.5-inch display, full metal design and a fingerprint scanner for added security. The battery isn't incredible on the Nokia 6, but it's not awful either making this one of our favorite affordable phones on the market right now.", price: "Price: 199.9", isFavourite: false)
    
    static let motoE4PlusDetail = DetailScene.setDetailMobile.ViewModel.MobileDataToList(thumbImageURL: "https://cdn.mos.cms.futurecdn.net/grwJkAGWQp4EPpWA3ys3YC-650-80.jpg", brand: "Samsung", rating: "Rating: 4.9", id: 1, name: "Moto E4 Plus", description: "First place in our list goes to the excellent Moto E4 Plus. It's a cheap phone that features phenomenal battery life, a fingerprint scanner and a premium feel design, plus it's a lot cheaper than the Moto G5 below. It is a little limited with its power, but it makes up for it by being able to last for a whole two days from a single charge. If price and battery are the most important features for you, the Moto E4 Plus will suit you perfectly.", price: "Price: 179.99", isFavourite: false)

    static let imageMobiles = MobileImagesFromAPI(mobileID: 2, url: "https://www.91-img.com/gallery_images_uploads/e/5/e5d5dd05841489a766156c845cb90d55d6dafe10.jpg", id: 2)
    
    static let imagesMobile: ResponseMobileImagesFromAPI = [MobileImagesFromAPI(mobileID: 2, url: "https://www.91-img.com/gallery_images_uploads/e/5/e5d5dd05841489a766156c845cb90d55d6dafe10.jpg", id: 2
      ), MobileImagesFromAPI(mobileID: 8, url: "http://www.91-img.com/pictures/nokia-6-mobile-phone-hres-2.jpg", id: 2
      ), MobileImagesFromAPI(mobileID: 9, url: "www.91-img.com/pictures/nokia-6-mobile-phone-hres-3.jpg", id: 2 )]
    
    static let imagesMobileWithHTTP: ResponseMobileImagesFromAPI = [MobileImagesFromAPI(mobileID: 2, url: "https://www.91-img.com/gallery_images_uploads/e/5/e5d5dd05841489a766156c845cb90d55d6dafe10.jpg", id: 2
      ), MobileImagesFromAPI(mobileID: 8, url: "http://www.91-img.com/pictures/nokia-6-mobile-phone-hres-2.jpg", id: 2
      ), MobileImagesFromAPI(mobileID: 9, url: "http://www.91-img.com/pictures/nokia-6-mobile-phone-hres-3.jpg", id: 2 )]
    
//    static let imagesMobileWithHTTPs: [DetailScene.getImagesWithIdFromAPI.ViewModel.MobileImagesFromAPI] = [MobileImagesFromAPI(mobileID: 2, url: "https://www.91-img.com/gallery_images_uploads/e/5/e5d5dd05841489a766156c845cb90d55d6dafe10.jpg", id: 2
//      ), MobileImagesFromAPI(mobileID: 8, url: "http://www.91-img.com/pictures/nokia-6-mobile-phone-hres-2.jpg", id: 2
//      ), MobileImagesFromAPI(mobileID: 9, url: "http://www.91-img.com/pictures/nokia-6-mobile-phone-hres-3.jpg", id: 2 )]

    
    static let image1ID2 = MobileImagesFromAPI(mobileID: 2, url: "https://www.91-img.com/gallery_images_uploads/e/5/e5d5dd05841489a766156c845cb90d55d6dafe10.jpg", id: 2)
    
    static let image2ID2 = MobileImagesFromAPI(mobileID: 8, url: "http://www.91-img.com/pictures/nokia-6-mobile-phone-hres-2.jpg", id: 2)
    
    static let image3ID2 = MobileImagesFromAPI(mobileID: 9, url: "http://www.91-img.com/pictures/nokia-6-mobile-phone-hres-3.jpg", id: 2)
    
    static let image1ID2vm = DetailScene.getImagesWithIdFromAPI.ViewModel.MobileImagesFromAPI(mobileID: 2, url: "https://www.91-img.com/gallery_images_uploads/e/5/e5d5dd05841489a766156c845cb90d55d6dafe10.jpg", id: 2)
    
    static let image2ID2vm = DetailScene.getImagesWithIdFromAPI.ViewModel.MobileImagesFromAPI(mobileID: 8, url: "http://www.91-img.com/pictures/nokia-6-mobile-phone-hres-2.jpg", id: 2)
    
    static let image3ID2vm = DetailScene.getImagesWithIdFromAPI.ViewModel.MobileImagesFromAPI(mobileID: 9, url: "http://www.91-img.com/pictures/nokia-6-mobile-phone-hres-3.jpg", id: 2)
  }
}

//{
//  "id": 2,
//  "url": "https://www.91-img.com/gallery_images_uploads/e/5/e5d5dd05841489a766156c845cb90d55d6dafe10.jpg",
//  "mobile_id": 2
//},
//{
//  "id": 8,
//  "url": "http://www.91-img.com/pictures/nokia-6-mobile-phone-hres-2.jpg",
//  "mobile_id": 2
//},
//{
//  "id": 9,
//  "url": "www.91-img.com/pictures/nokia-6-mobile-phone-hres-3.jpg",
//  "mobile_id": 2
//}

//"id": 2,
//"rating": 4.6,
//"brand": "Nokia",
//"name": "Nokia 6",
//"price": 199.99,
//"thumbImageURL": "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg",
//"description": "Nokia is back in the mobile phone game and after a small price drop to the Nokia 6 we've now seen it enter our best cheap phone list. It comes with a Full HD 5.5-inch display, full metal design and a fingerprint scanner for added security. The battery isn't incredible on the Nokia 6, but it's not awful either making this one of our favorite affordable phones on the market right now."

//"id": 4,
//"rating": 3.8,
//"brand": "Motorola",
//"name": "Moto G5",
//"price": 165,
//"thumbImageURL": "https://cdn.mos.cms.futurecdn.net/DcUtY6YfhoajHnoKUgGFqn-650-80.jpg",
//"description": "Motorola's Moto G5, a former best cheap phone in the world, has slipped a few places thanks to better priced alternatives, plus the introduction of the new G5S. The Moto G5 comes with a metal design, 1080p display and fingerprint scanner. You won't get the fastest chipset on this list or NFC with the Moto G5, but as an all-round product the cheap Motorola phone will serve you well."

//"id": 1,
//"rating": 4.9,
//"brand": "Samsung",
//"name": "Moto E4 Plus",
//"price": 179.99,
//"thumbImageURL": "https://cdn.mos.cms.futurecdn.net/grwJkAGWQp4EPpWA3ys3YC-650-80.jpg",
//"description": "First place in our list goes to the excellent Moto E4 Plus. It's a cheap phone that features phenomenal battery life, a fingerprint scanner and a premium feel design, plus it's a lot cheaper than the Moto G5 below. It is a little limited with its power, but it makes up for it by being able to last for a whole two days from a single charge. If price and battery are the most important features for you, the Moto E4 Plus will suit you perfectly."
