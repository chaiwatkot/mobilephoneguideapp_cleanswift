//
//  ListScenePresenterTests.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 5/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

@testable import MobileListGuide
import XCTest

class ListScenePresenterTests: XCTestCase {
  
  // MARK: - Subject under test
  
  var sut: ListScenePresenter!
  var displaySpy: ListSceneDisplaySpy!
  var mobiledata: [MobileResponse]!

  // MARK: - Test lifecycle
  
  override func setUp() {
    super.setUp()
    setupListScenePresenter()
    displaySpy = ListSceneDisplaySpy()
    sut.viewController = displaySpy
    mobiledata = [Seeds.MobileList.motoE4Plus,Seeds.MobileList.motoG5,Seeds.MobileList.nokia6]
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  // MARK: - Test setup
  
  func setupListScenePresenter() {
    sut = ListScenePresenter()
  } 
  
  // MARK: - Test doubles
  class ListSceneDisplaySpy: ListSceneViewControllerInterface {
    
    var displayMobileListOnTableCalled = false
    var displayMobileListCalled = false
    var displaySetDisplayDataCalled = false
    
    var viewModeldisplayMobileListOnTable: ListScene.GetMobileListData.ViewModel!
    var viewModeldisplaySetDisplayData: ListScene.GetMobileListData.ViewModel!
    
    func displayMobileListOnTable(viewModel: ListScene.GetMobileListData.ViewModel) {
      displayMobileListOnTableCalled = true
      self.viewModeldisplayMobileListOnTable = viewModel
    }
    
    func displaySetDisplayData(viewModel: ListScene.GetMobileListData.ViewModel) {
      displaySetDisplayDataCalled = true
      self.viewModeldisplaySetDisplayData = viewModel
    }
  }
  
  // MARK: - Tests
  
  func testPresenterDisplayMobileListOnTableAskViewController() {
    // Given
    // When
    let response = ListScene.GetMobileListData.Response(mobileListResponse: mobiledata)
    sut.presentMobileList(response: response)
    
    // Then
    let displayMobileDatas = displaySpy.viewModeldisplayMobileListOnTable.displayListMobile
    XCTAssert(displayMobileDatas[0] == Seeds.MobileList.motoE4PlusDisplay)
    XCTAssert(displayMobileDatas[1] == Seeds.MobileList.motoG5Display)
    XCTAssert(displayMobileDatas[2] == Seeds.MobileList.nokia6Display)
    XCTAssert(displaySpy.displayMobileListOnTableCalled)
  }
  
  func testPresenterDisplaySetDisplayDataAskViewController() {
    // Given
    // When
    let response = ListScene.reloadSetFavourite.Response(mobileListResponse: mobiledata)
    sut.presentSetMobileDataIsFavourite(response: response)
    // Then
    let displayMobileDatas = displaySpy.viewModeldisplaySetDisplayData.displayListMobile
    XCTAssert(displayMobileDatas[0] == Seeds.MobileList.motoE4PlusDisplay)
    XCTAssert(displayMobileDatas[1] == Seeds.MobileList.motoG5Display)
    XCTAssert(displayMobileDatas[2] == Seeds.MobileList.nokia6Display)
    XCTAssert(displaySpy.displaySetDisplayDataCalled)
  }
  
  func testPresenterMobileListIsFavouriteToDisplayMobileListOnTableAskViewController() {
    // Given
    // When
    let response = ListScene.SetFavourite.Response(mobileListResponse: mobiledata)
    sut.presentMobileListIsFavourite(response: response)
    // Then
    let displayMobileDatas = displaySpy.viewModeldisplayMobileListOnTable.displayListMobile
    XCTAssert(displayMobileDatas[0] == Seeds.MobileList.motoE4PlusDisplay)
    XCTAssert(displayMobileDatas[1] == Seeds.MobileList.motoG5Display)
    XCTAssert(displayMobileDatas[2] == Seeds.MobileList.nokia6Display)
    XCTAssert(displaySpy.displayMobileListOnTableCalled)
  }
  
  func testPresenterMobileListIsSortToDisplayMobileListOnTableAskViewController() {
    // Given
    // When
    let response = ListScene.SortMobileList.Response(mobileListResponse: mobiledata)
    sut.presentMobileListIsSort(response: response)
    // Then
    let displayMobileDatas = displaySpy.viewModeldisplayMobileListOnTable.displayListMobile
    XCTAssert(displayMobileDatas[0] == Seeds.MobileList.motoE4PlusDisplay)
    XCTAssert(displayMobileDatas[1] == Seeds.MobileList.motoG5Display)
    XCTAssert(displayMobileDatas[2] == Seeds.MobileList.nokia6Display)
    XCTAssert(displaySpy.displayMobileListOnTableCalled)
  }
}

