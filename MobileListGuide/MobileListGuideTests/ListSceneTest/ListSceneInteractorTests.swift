//
//  ListSceneInteractorTests.swift
//  MobileListGuide
//
//  Created by Chaiwat Chanthasen on 5/9/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

@testable import MobileListGuide
import XCTest

class ListSceneInteractorTests: XCTestCase {

  // MARK: - Subject under test

  var sut: ListSceneInteractor!
  var workerSpy: ListSceneWorkerSpy!
  var presentationSpy: ListScenePresentationSpy!
  // MARK: - Test lifecycle

  override func setUp() {
    super.setUp()
    setupListSceneInteractor()
    presentationSpy = ListScenePresentationSpy()
    sut.presenter = presentationSpy
    workerSpy = ListSceneWorkerSpy(store: ListSceneStore())
    sut.worker = workerSpy

  }

  override func tearDown() {
    super.tearDown()
  }

  // MARK: - Test setup

  func setupListSceneInteractor() {
    sut = ListSceneInteractor()
      }

  // MARK: - Test doubles
  class ListScenePresentationSpy: ListScenePresenterInterface {

    var presentMobileListCalled = false
    var presentMobileListIsFavouriteCalled = false
    var presentMobileListIsSortCalled = false
    var presentSetMobileDataIsFavouriteCalled = false
    
    var responsePresentMobileList: ListScene.GetMobileListData.Response!
    var responsePresentMobileListIsFavourite: ListScene.SetFavourite.Response!
    var responsePresentMobileListIsSort: ListScene.SortMobileList.Response!
    var responsePresentSetMobileDataIsFavourite: ListScene.reloadSetFavourite.Response!
    
    func presentMobileList(response: ListScene.GetMobileListData.Response) {
      presentMobileListCalled = true
      responsePresentMobileList = response
    }
    
    func presentMobileListIsFavourite(response: ListScene.SetFavourite.Response) {
      presentMobileListIsFavouriteCalled = true
      responsePresentMobileListIsFavourite = response
    }
    
    func presentMobileListIsSort(response: ListScene.SortMobileList.Response) {
      presentMobileListIsSortCalled = true
      responsePresentMobileListIsSort = response
    }
    
    func presentSetMobileDataIsFavourite(response: ListScene.reloadSetFavourite.Response) {
      presentSetMobileDataIsFavouriteCalled = true
      responsePresentSetMobileDataIsFavourite = response
    }
  }
  
  class ListSceneWorkerSpy: ListSceneWorker {
    
    var getMobileListFromApiCalled = false
    var forcefailure = false
    
    override func getMobileListFromApi(_ completion: @escaping ([MobileResponse]) -> Void) {
      // NOTE: Do the work
      getMobileListFromApiCalled = true
      if forcefailure {
        completion([])
      } else {
        completion([Seeds.MobileList.motoE4Plus, Seeds.MobileList.motoG5, Seeds.MobileList.nokia6])
      }
    }
  }

  // MARK: - Tests

  func testGetMobileDataListFromApi() {
    // Given
    // When
    let request = ListScene.GetMobileListData.Request()
    sut.getMobilelist(request: request)
    // Then
    let responsePresentGetMobileData = presentationSpy.responsePresentMobileList.mobileListResponse
    XCTAssert(responsePresentGetMobileData[0] == Seeds.MobileList.motoE4Plus)
    XCTAssert(responsePresentGetMobileData[1] == Seeds.MobileList.motoG5)
    XCTAssert(responsePresentGetMobileData[2] == Seeds.MobileList.nokia6)
    XCTAssert(workerSpy.getMobileListFromApiCalled, "getMobilelistFromApi() should ask ListSceneWorker to getMobileListData")
    XCTAssert(presentationSpy.presentMobileListCalled, "presentMobileListCalled() should ask presenter to format mobileList result")
  }
  
  func testGetMobileDataListFromApiFailure() {
    // Given
    workerSpy.forcefailure = true
    // When
    let request = ListScene.GetMobileListData.Request()
    sut.getMobilelist(request: request)
    // Then
    let responsePresentGetMobileData = presentationSpy.responsePresentMobileList.mobileListResponse
    XCTAssert(responsePresentGetMobileData == [])
    XCTAssert(workerSpy.getMobileListFromApiCalled, "getMobilelistFromApi() should ask ListSceneWorker to getMobileListData")
    XCTAssert(presentationSpy.presentMobileListCalled, "presentMobileListCalled() should ask presenter to format mobileList result")
  }
  
  func testMobileListIsFavourite() {
    // Given
    // When
    let requestGetMobile = ListScene.GetMobileListData.Request()
    sut.getMobilelist(request: requestGetMobile)
    let requestSetFavourite = ListScene.SetFavourite.Request(index: 0)
    sut.setIsFavourite(request: requestSetFavourite)
    // Then
    let responsePresentMobileIsFavourite = presentationSpy.responsePresentMobileListIsFavourite.mobileListResponse
    XCTAssert(responsePresentMobileIsFavourite[0].isFavourite)
    XCTAssert(workerSpy.getMobileListFromApiCalled, "getMobilelistFromApi() should ask ListSceneWorker to getMobileListData")
    XCTAssert(presentationSpy.presentMobileListIsFavouriteCalled, "presentMobileListCalled() should ask presenter to format mobileList result")
  }
  
  func testMobileIsSortLowToHigh() {
    // Given
    // When
    let requestGetData = ListScene.GetMobileListData.Request()
    sut.getMobilelist(request: requestGetData)
    let requestSort = ListScene.SortMobileList.Request(sortCase: .lowToHigh)
    sut.setSortMobileList(request: requestSort)
    // Then
    let responsePresentSortMobileData = presentationSpy.responsePresentMobileListIsSort.mobileListResponse
    XCTAssert(responsePresentSortMobileData[0].price < responsePresentSortMobileData[1].price)
    XCTAssert(responsePresentSortMobileData[1].price < responsePresentSortMobileData[2].price)
    XCTAssert(workerSpy.getMobileListFromApiCalled, "getMobilelistFromApi() should ask ListSceneWorker to getMobileListData")
    XCTAssert(presentationSpy.presentMobileListIsSortCalled, "presentMobileListCalled() should ask presenter to format mobileList result")
  }
  
  func testMobileIsSortHighToLow() {
    // Given
    // When
    let requestGetData = ListScene.GetMobileListData.Request()
    sut.getMobilelist(request: requestGetData)
    let requestSort = ListScene.SortMobileList.Request(sortCase: .highToLow)
    sut.setSortMobileList(request: requestSort)
    // Then
    let responsePresentGetMobileData = presentationSpy.responsePresentMobileListIsSort.mobileListResponse
    XCTAssert(responsePresentGetMobileData[0].price > responsePresentGetMobileData[1].price)
    XCTAssert(responsePresentGetMobileData[1].price > responsePresentGetMobileData[2].price)
    XCTAssert(workerSpy.getMobileListFromApiCalled, "getMobilelistFromApi() should ask ListSceneWorker to getMobileListData")
    XCTAssert(presentationSpy.presentMobileListIsSortCalled, "presentMobileListCalled() should ask presenter to format mobileList result")
  }
  
  func testMobileIsSortRating() {
    // Given
    // When
    let requestGetData = ListScene.GetMobileListData.Request()
    sut.getMobilelist(request: requestGetData)
    let requestSort = ListScene.SortMobileList.Request(sortCase: .rating)
    sut.setSortMobileList(request: requestSort)
    // Then
    let responsePresentGetMobileData = presentationSpy.responsePresentMobileListIsSort.mobileListResponse
    XCTAssert(responsePresentGetMobileData[0].rating > responsePresentGetMobileData[1].rating)
    XCTAssert(responsePresentGetMobileData[1].rating > responsePresentGetMobileData[2].rating)
    XCTAssert(workerSpy.getMobileListFromApiCalled, "getMobilelistFromApi() should ask ListSceneWorker to getMobileListData")
    XCTAssert(presentationSpy.presentMobileListIsSortCalled, "presentMobileListCalled() should ask presenter to format mobileList result")
  }
  
  func testSetMobileDataIsFavourite() {
    // Given
    // When
    let requestGetData = ListScene.GetMobileListData.Request()
    sut.getMobilelist(request: requestGetData)
    let requestReload = ListScene.reloadSetFavourite.Request()
    sut.reloadSetFavourite(requset: requestReload)
    // Then
    let responsePresentGetMobileData = presentationSpy.responsePresentSetMobileDataIsFavourite.mobileListResponse
    XCTAssert(responsePresentGetMobileData[0] == Seeds.MobileList.motoE4Plus)
    XCTAssert(responsePresentGetMobileData[1] == Seeds.MobileList.motoG5)
    XCTAssert(responsePresentGetMobileData[2] == Seeds.MobileList.nokia6)
    XCTAssert(workerSpy.getMobileListFromApiCalled, "getMobilelistFromApi() should ask ListSceneWorker to getMobileListData")
    XCTAssert(presentationSpy.presentSetMobileDataIsFavouriteCalled, "presentMobileListCalled() should ask presenter to format mobileList result")
  }
  
  func testDeleteFavourite() {
    // Given
    // When
    let requestGetData = ListScene.GetMobileListData.Request()
    sut.getMobilelist(request: requestGetData)
    let requestDeleteFavourite = ListScene.DeleteFavourite.Request(id: 1, index: 0)
    sut.deleteFavourite(request: requestDeleteFavourite)
    // Then
    let responsePresentGetMobileData = presentationSpy.responsePresentSetMobileDataIsFavourite.mobileListResponse
    XCTAssert(responsePresentGetMobileData[0].isFavourite)
    XCTAssert(workerSpy.getMobileListFromApiCalled, "getMobilelistFromApi() should ask ListSceneWorker to getMobileListData")
    XCTAssert(presentationSpy.presentMobileListCalled, "presentMobileListCalled() should ask presenter to format mobileList result")
  }

}
